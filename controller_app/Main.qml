import QtQuick
import QtQuick.Controls

ApplicationWindow {
    id: root

    width: 900
    minimumWidth: 900
    maximumWidth: 900
    height: 400
    minimumHeight: 100
    flags: Qt.WindowCloseButtonHint
    visible: true
    title: "Controllers Manager"

    SensorComponent {
        id: sensors_component

        anchors.top: parent.top
        height: parent.height
        width: 300
    }

    ControllerComponent {
        id: controller_compoenent

        anchors.top: parent.top
        anchors.left: sensors_component.right
        height: parent.height
        width: 300
    }

    ActuatorComponent {
        id: actuator_component

        anchors.top: parent.top
        anchors.left: controller_compoenent.right
        height: parent.height
        width: 300
    }
}
