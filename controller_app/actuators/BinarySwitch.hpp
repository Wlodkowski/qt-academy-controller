#ifndef BINARYSWITCH_HPP
#define BINARYSWITCH_HPP

#include "IBinaryActuator.hpp"
#include "ActuatorBase.hpp"

#include <QString>

class BinarySwitch : public IBinaryActuator, public ActuatorBase
{
public:
    BinarySwitch(QString id);

    // implementation of IBinaryActuator
    void switchOpen() override;
    void switchClose() override;
    SwitchState getState() const override;

    // implementation of ActuatorBase
    QString printState() const override;

private:
    SwitchState state_;
};

#endif // BINARYSWITCH_HPP
