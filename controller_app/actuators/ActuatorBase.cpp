#include "ActuatorBase.hpp"

ActuatorBase::ActuatorBase(QString id) :
    id_(id)
{
}

QString ActuatorBase::printId() const
{
    return id_;
}
