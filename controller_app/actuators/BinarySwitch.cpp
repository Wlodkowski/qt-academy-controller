#include "BinarySwitch.hpp"

BinarySwitch::BinarySwitch(QString id) :
    ActuatorBase(id),
    state_(SwitchState::close)
{
}

void BinarySwitch::switchOpen()
{
    state_ = SwitchState::open;
}

void BinarySwitch::switchClose()
{
    state_ = SwitchState::close;
}

SwitchState BinarySwitch::getState() const
{
    return state_;
}

QString BinarySwitch::printState() const
{
    switch(state_)
    {
    case SwitchState::open:
        return "switch open";
    case SwitchState::close:
        return "switch close";
    case SwitchState::unknown:
    default:
        return "switch unknown";
    }
}
