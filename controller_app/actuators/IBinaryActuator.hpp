#ifndef IBINARYACTUATOR_HPP
#define IBINARYACTUATOR_HPP

enum class SwitchState
{
    unknown = 0,
    open,
    close
};

class IBinaryActuator
{
public:
    virtual void switchOpen() = 0;
    virtual void switchClose() = 0;
    virtual SwitchState getState() const = 0;

    ~IBinaryActuator() = default;
};

#endif // IBINARYACTUATOR_HPP
