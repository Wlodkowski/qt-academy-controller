#ifndef IACTUATOR_HPP
#define IACTUATOR_HPP

#include <QString>

class IActuator
{
public:
    virtual QString printId() const = 0;
    virtual QString printState() const = 0;

    virtual ~IActuator() = default;
};

#endif // IACTUATOR_HPP
