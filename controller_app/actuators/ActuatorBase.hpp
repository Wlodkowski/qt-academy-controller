#ifndef ACTUATORBASE_HPP
#define ACTUATORBASE_HPP

#include "IActuator.hpp"

#include <QString>

class ActuatorBase : public IActuator
{
public:
    ActuatorBase(QString id);

    virtual QString printId() const;
    virtual QString printState() const = 0;

    virtual ~ActuatorBase() = default;

private:
    QString id_;
};

#endif // ACTUATORBSAE_HPP
