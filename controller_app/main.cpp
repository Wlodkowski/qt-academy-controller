#include "controllers/ControllersManager.hpp"
#include "controllers/BinaryTemperatureController.hpp"
#include "sensors/SimulatedTemperatureSensor.hpp"
#include "actuators/BinarySwitch.hpp"

#include "gui/GuiComponent.hpp"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <memory>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    GuiComponent gui;

    engine.rootContext()->setContextProperty("gui", &gui);

    auto living_room_temperature = std::make_shared<SimulatedTemperatureSensor>("Living room temperature");
    auto bedroom_1_temperature = std::make_shared<SimulatedTemperatureSensor>("Bedroom 1 temperature");
    auto bedroom_2_temperature = std::make_shared<SimulatedTemperatureSensor>("Bedroom 2 temperature");;
    auto kitchen_temperature = std::make_shared<SimulatedTemperatureSensor>("Kitchen temperature");

    auto living_room_heater = std::make_shared<BinarySwitch>("Living room heater");
    auto bedroom_1_heater = std::make_shared<BinarySwitch>("Bedroom 1 heater");;
    auto bedroom_2_heater = std::make_shared<BinarySwitch>("Bedroom 2 heater");;
    auto kitchen_heater = std::make_shared<BinarySwitch>("Kitchen heater");

    auto living_room_temperature_controller = std::make_shared<BinaryTemperatureController>("Living room temperature controller",
                                                                                            ControllerInterval::interval_100ms,
                                                                                            living_room_temperature,
                                                                                            living_room_heater,
                                                                                            22.0f,
                                                                                            0.5f);

    auto bedroom_1_temperature_controller = std::make_shared<BinaryTemperatureController>("Bedroom 1 temperature controller",
                                                                                          ControllerInterval::interval_100ms,
                                                                                          bedroom_1_temperature,
                                                                                          bedroom_1_heater,
                                                                                          21.0f,
                                                                                          1.0f);

    auto bedroom_2_temperature_controller = std::make_shared<BinaryTemperatureController>("Bedroom 2 temperature controller",
                                                                                          ControllerInterval::interval_100ms,
                                                                                          bedroom_2_temperature,
                                                                                          bedroom_2_heater,
                                                                                          21.0f,
                                                                                          1.0f);

    auto kitchen_temperature_controller = std::make_shared<BinaryTemperatureController>("Kitchen temperature controller",
                                                                                        ControllerInterval::interval_100ms,
                                                                                        kitchen_temperature,
                                                                                        kitchen_heater,
                                                                                        20.0f,
                                                                                        1.0f);

    ControllersManager controllers;

    controllers.addController(living_room_temperature_controller);
    controllers.addController(bedroom_1_temperature_controller);
    controllers.addController(bedroom_2_temperature_controller);
    controllers.addController(kitchen_temperature_controller);

    gui.getSensors()->onAdded(living_room_temperature);
    gui.getSensors()->onAdded(bedroom_1_temperature);
    gui.getSensors()->onAdded(bedroom_2_temperature);
    gui.getSensors()->onAdded(kitchen_temperature);

    gui.getActuators()->onAdded(living_room_heater);
    gui.getActuators()->onAdded(bedroom_1_heater);
    gui.getActuators()->onAdded(bedroom_2_heater);
    gui.getActuators()->onAdded(kitchen_heater);

    gui.getControllers()->onAdded(living_room_temperature_controller);
    gui.getControllers()->onAdded(bedroom_1_temperature_controller);
    gui.getControllers()->onAdded(bedroom_2_temperature_controller);
    gui.getControllers()->onAdded(kitchen_temperature_controller);

    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.loadFromModule("controller_app", "Main");

    controllers.startAll();

    return app.exec();
}
