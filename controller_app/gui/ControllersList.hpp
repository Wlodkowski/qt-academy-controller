#ifndef CONTROLLERSLIST_HPP
#define CONTROLLERSLIST_HPP

#include "ListBase.hpp"
#include "controllers/IController.hpp"

class ControllersList : public ListBase<IController>
{
    Q_OBJECT
    QML_ELEMENT

public:
    ControllersList();

private:
    QVariant serialize(int index) const;
};

#endif // CONTROLLERSLIST_HPP
