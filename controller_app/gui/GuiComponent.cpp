#include "GuiComponent.hpp"

GuiComponent::GuiComponent() :
    actuators_(),
    controllers_(),
    sensors_(),
    timer_()

{
    actuators_.setParent(this);
    controllers_.setParent(this);
    sensors_.setParent(this);

    QQmlEngine::setObjectOwnership(&actuators_, QQmlEngine::CppOwnership);
    QQmlEngine::setObjectOwnership(&controllers_, QQmlEngine::CppOwnership);
    QQmlEngine::setObjectOwnership(&sensors_, QQmlEngine::CppOwnership);

    timer_.setInterval(200);    // 200ms
    timer_.callOnTimeout(this, &GuiComponent::onTimerTimeout);
    timer_.start();
}

void GuiComponent::onTimerTimeout()
{
    actuators_.refresh();
    controllers_.refresh();
    sensors_.refresh();
}
