#include "ActuatorsList.hpp"

ActuatorsList::ActuatorsList() :
    ListBase("ActuatorsList")
{
}

QVariant ActuatorsList::serialize(int index) const
{
    if(elements_[index] != nullptr)
    {
        auto s = elements_[index];
        // TODO: consider different object types - maybe a QJsonObject would be better?
        QMap<QString, QVariant> properties;
        properties.insert("id", s->printId());
        properties.insert("state", s->printState());
        // properties.insert("value", s->printValue());
        // properties.insert("unit", s->printUnit());

        return properties;
    }
    return QVariant();
}
