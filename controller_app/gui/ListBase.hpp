#ifndef LISTBASE_H
#define LISTBASE_H

#include <QAbstractListModel>
#include <QQmlEngine>

#include <vector>

template <typename InterfaceType>
class ListBase : public QAbstractListModel
{
public:
    ListBase(QString id);
    virtual ~ListBase() = default;

    void onAdded(std::shared_ptr<InterfaceType> object);
    void onRemoved(const QString& id);
    void refresh();

    Q_INVOKABLE QString getId();

    // implementation of QAbstractListModel
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    Q_INVOKABLE QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;
    virtual QVariant serialize(int index) const = 0;
    bool isOnList(const QString& id) const;

    QString id_;
    std::vector<std::shared_ptr<InterfaceType>> elements_;
};

template <typename InterfaceType>
ListBase<InterfaceType>::ListBase(QString id) :
    id_(id)
{
}

template <typename InterfaceType>
void ListBase<InterfaceType>::onAdded(std::shared_ptr<InterfaceType> object)
{
    if(!isOnList(object->printId()))
    {
        elements_.push_back(object);
    }
    refresh();
}

template <typename InterfaceType>
void ListBase<InterfaceType>::onRemoved(const QString& id)
{
    for(auto e = elements_.begin(); e != elements_.end(); ++e)
    {
        if(e->printId() == id)
        {
            elements_.erase(e);
        }
    }
    refresh();
}

template <typename InterfaceType>
void ListBase<InterfaceType>::refresh()
{
    QModelIndex top_left = index(0, 0);
    QModelIndex bottom_right = index(elements_.size() - 1, 0);
    emit dataChanged(top_left, bottom_right);
}

template <typename InterfaceType>
QVariant ListBase<InterfaceType>::headerData(int section, Qt::Orientation /*orientation*/, int /*role*/) const
{
    if(section == 0)
    {
        return id_;
    }
    return "Unknown";
}

template <typename InterfaceType>
int ListBase<InterfaceType>::rowCount(const QModelIndex& parent) const
{
    return elements_.size();
}

template <typename InterfaceType>
QVariant ListBase<InterfaceType>::data(const QModelIndex& index, int role) const
{
    if(!index.isValid() || index.row() < 0 || index.row() >= elements_.size())
    {
        return QVariant();
    }
    return serialize(index.row());
}

template <typename InterfaceType>
QHash<int, QByteArray> ListBase<InterfaceType>::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles[Qt::DisplayRole] = "list_data";
    return roles;
}

template <typename InterfaceType>
bool ListBase<InterfaceType>::isOnList(const QString& id) const
{
    for(auto e : elements_)
    {
        if(e->printId() == id)
        {
            return true;
        }
    }
    return false;
}

#endif // LISTBASE_H
