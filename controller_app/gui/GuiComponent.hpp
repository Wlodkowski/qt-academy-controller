#ifndef GUICOMPONENT_HPP
#define GUICOMPONENT_HPP

#include "ActuatorsList.hpp"
#include "ControllersList.hpp"
#include "SensorsList.hpp"

#include <QTimer>

class GuiComponent : public QObject
{
    Q_OBJECT

public:
    GuiComponent();

    Q_INVOKABLE ActuatorsList* getActuators() {return &actuators_;}
    Q_INVOKABLE SensorsList* getSensors() {return &sensors_;}
    Q_INVOKABLE ControllersList* getControllers() {return &controllers_;}

    void onTimerTimeout();

private:
    ActuatorsList actuators_;
    ControllersList controllers_;
    SensorsList sensors_;

    QTimer timer_;
};

#endif // GUICOMPONENT_HPP
