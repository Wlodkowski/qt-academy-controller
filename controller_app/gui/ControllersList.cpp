#include "ControllersList.hpp"

ControllersList::ControllersList() :
    ListBase("ControllersList")
{
}

QVariant ControllersList::serialize(int index) const
{
    if(elements_[index] != nullptr)
    {
        auto s = elements_[index];
        // TODO: consider different object types - maybe a QJsonObject would be better?
        QMap<QString, QVariant> properties;
        properties.insert("id", s->printId());
        properties.insert("interval", s->printInterval());
        properties.insert("state", s->isRunning() ? "running" : "stopped");
        // properties.insert("unit", s->printUnit());

        return properties;
    }
    return QVariant();
}
