#include "SensorsList.hpp"

SensorsList::SensorsList() :
    ListBase("SensorsList")
{
}

QVariant SensorsList::serialize(int index) const
{
    if(elements_[index] != nullptr)
    {
        auto s = elements_[index];
        // TODO: consider different object types - maybe a QJsonObject would be better?
        QMap<QString, QVariant> properties;
        properties.insert("id", s->printId());
        properties.insert("type", s->printValueType());
        properties.insert("value", s->printValue());
        properties.insert("unit", s->printUnit());

        return properties;
    }
    return QVariant();
}
