#ifndef SENSORSLIST_HPP
#define SENSORSLIST_HPP

#include "ListBase.hpp"
#include "sensors/ISensor.hpp"

class SensorsList : public ListBase<ISensor>
{
    Q_OBJECT
    QML_ELEMENT

public:
    SensorsList();
    virtual ~SensorsList() = default;

private:
    QVariant serialize(int index) const override;
};

#endif // SENSORSLIST_HPP
