#ifndef ACTUATORSLIST_HPP
#define ACTUATORSLIST_HPP

#include "ListBase.hpp"
#include "actuators/IActuator.hpp"

class ActuatorsList : public ListBase<IActuator>
{
    Q_OBJECT
    QML_ELEMENT

public:
    ActuatorsList();
    virtual ~ActuatorsList() = default;

private:
    QVariant serialize(int index) const override;
};


#endif // ACTUATORSLIST_HPP
