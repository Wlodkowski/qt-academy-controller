#include "ControllerBase.hpp"

ControllerBase::ControllerBase(QString id, ControllerInterval interval) :
    id_(id),
    interval_ms_(static_cast<qint64>(interval))
{
}

bool ControllerBase::run(QTime current_time)
{
    if(isRunning())
    {
        if(last_run_.isNull() == true || last_run_.addMSecs(interval_ms_) >= current_time)
        {
            last_run_ = current_time;
            process();
            return true;
        }
    }
    return false;
}

QString ControllerBase::printId() const
{
    return id_;
}

QString ControllerBase::printInterval() const
{
    return QString::number(interval_ms_) + "ms";
}

qint64 ControllerBase::getIntervalMs() const
{
    return interval_ms_;
}
