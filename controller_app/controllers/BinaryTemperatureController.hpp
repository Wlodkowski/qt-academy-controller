#ifndef BINARYTEMPERATURECONTROLLER_HPP
#define BINARYTEMPERATURECONTROLLER_HPP

#include "ControllerBase.hpp"
#include "actuators/IBinaryActuator.hpp"
#include "sensors/IAnalogSensor.hpp"

class BinaryTemperatureController : public ControllerBase
{
public:
    BinaryTemperatureController() = delete;
    BinaryTemperatureController(QString id,
                                ControllerInterval interval,
                                const std::shared_ptr<IAnalogSensor> sensor,
                                std::shared_ptr<IBinaryActuator> output,
                                float threshold,
                                float histeresis);

    void setThreshold(float threshold);
    void setHisteresis(float histeresis);

    void start() override;
    void stop() override;
    bool isRunning() const override;
    void process() override;
    QString printProperties() const override;

private:
    const std::shared_ptr<IAnalogSensor> sensor_;
    std::shared_ptr<IBinaryActuator> switch_;

    float threshold_;
    float histeresis_;

    bool is_enabled_;
};

#endif // BINARYTEMPERATURECONTROLLER_HPP
