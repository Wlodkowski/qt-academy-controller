#ifndef ICONTROLLER_HPP
#define ICONTROLLER_HPP

#include <QString>

class IController
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;

    virtual bool isRunning() const = 0;

    virtual QString printId() const = 0;
    virtual QString printInterval() const = 0;

    virtual ~IController() = default;
};

#endif // ICONTROLLER_HPP
