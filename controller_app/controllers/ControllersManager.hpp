#ifndef CONTROLLERSMANAGER_HPP
#define CONTROLLERSMANAGER_HPP

#include "ControllerBase.hpp"

#include <QTimer>

#include <vector>
#include <mutex>
#include <memory>

class ControllersManager : public QObject
{
    Q_OBJECT

public:
    ControllersManager();

    bool addController(std::shared_ptr<ControllerBase> controller);
    bool removeController(QString controller_id);

    void startAll();
    void stopAll();

//signals:
    void onTimerTimeout();

private:
    void calculateTimerInterval();

    std::vector<std::shared_ptr<ControllerBase>> controllers_;
    mutable std::mutex controllers_mutex_;

    QTimer timer_;
};

#endif // CONTROLLERSMANAGER_HPP
