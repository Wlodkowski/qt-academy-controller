#include "ControllersManager.hpp"

#include <QTime>

ControllersManager::ControllersManager() :
    controllers_(),
    timer_()
{
    timer_.callOnTimeout(this, &ControllersManager::onTimerTimeout);
}


bool ControllersManager::addController(std::shared_ptr<ControllerBase> controller)
{
    if(controller)
    {
        std::lock_guard<std::mutex> lock(controllers_mutex_);

        controllers_.push_back(std::move(controller));
        calculateTimerInterval();
        return true;
    }
    return false;
}

bool ControllersManager::removeController(QString controller_id)
{
    std::lock_guard<std::mutex> lock(controllers_mutex_);

    for(auto ctrl = controllers_.begin(); ctrl != controllers_.end(); ++ctrl)
    {
        if(ctrl->get()->printId() == controller_id)
        {
            controllers_.erase(ctrl);
            calculateTimerInterval();
            return true;
        }
    }
    return false;
}

void ControllersManager::startAll()
{
    std::lock_guard<std::mutex> lock(controllers_mutex_);

    for(auto& ctrl : controllers_)
    {
        ctrl->start();
    }
}

void ControllersManager::stopAll()
{
    std::lock_guard<std::mutex> lock(controllers_mutex_);

    for(auto& ctrl : controllers_)
    {
        ctrl->stop();
    }
}

void ControllersManager::onTimerTimeout()
{
    std::lock_guard<std::mutex> lock(controllers_mutex_);

    for(auto& ctrl : controllers_)
    {
        ctrl->run(QTime::currentTime());
    }
}

void ControllersManager::calculateTimerInterval()
{
    if(controllers_.empty())
    {
        timer_.stop();
        return;
    }

    // TODO: calculate timer interval as a greateast common divisor of controllers interval
    timer_.setInterval(50);

    if(!timer_.isActive())
    {
        timer_.start();
    }
}
