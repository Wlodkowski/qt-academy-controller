#ifndef CONTROLLER_BASE_HPP
#define CONTROLLER_BASE_HPP

#include "IController.hpp"

#include <QTime>
#include <QString>

enum class ControllerInterval
{
    interval_50ms  = 50,
    interval_100ms = 100,
    interval_150ms = 150,
    interval_200ms = 200,
    interval_250ms = 250,
    interval_300ms = 300,
    interval_350ms = 350,
    interval_400ms = 400,
    interval_450ms = 450,
    interval_500ms = 500,
    interval_1s    = 1000,
    interval_2s    = 2000,
    interval_5s    = 5000
};

class ControllerBase : public IController
{
public:
    ControllerBase(QString id, ControllerInterval interval);
    virtual ~ControllerBase() = default;

    bool run(QTime current_time);
    qint64 getIntervalMs() const;

    QString printId() const override;
    QString printInterval() const override;

    virtual QString printProperties() const = 0;

private:
    virtual void process() = 0;

    QString id_;
    qint64 interval_ms_;
    QTime last_run_;
};

#endif // ICONTROLLER_BASE_HPP
