#include "BinaryTemperatureController.hpp"

#include <QTextStream>

BinaryTemperatureController::BinaryTemperatureController(QString id,
                                                         ControllerInterval interval,
                                                         const std::shared_ptr<IAnalogSensor> sensor,
                                                         std::shared_ptr<IBinaryActuator> output,
                                                         float threshold,
                                                         float histeresis) :
    ControllerBase(id, interval),
    sensor_(sensor),
    switch_(output),
    threshold_(threshold),
    histeresis_(histeresis),
    is_enabled_(false)
{
}

void BinaryTemperatureController::setThreshold(float threshold)
{
    threshold_ = threshold;
}

void BinaryTemperatureController::setHisteresis(float histeresis)
{
    histeresis_ = histeresis;
}

void BinaryTemperatureController::start()
{
    is_enabled_ = true;
}

void BinaryTemperatureController::stop()
{
    is_enabled_ = false;
}

bool BinaryTemperatureController::isRunning() const
{
    return is_enabled_;
}

void BinaryTemperatureController::process()
{
    const auto temperature = sensor_->getSensorValue();
    const auto switch_state = switch_->getState();

    if(switch_state == SwitchState::open)
    {
        if(temperature >= threshold_ + histeresis_)
        {
            switch_->switchClose();
        }
    }
    else if (switch_state == SwitchState::close)
    {
        if(temperature <= threshold_ - histeresis_)
        {
            switch_->switchOpen();
        }
    }
}

QString BinaryTemperatureController::printProperties() const
{
    QTextStream stream;

    stream << "controller : {\n";
    stream << "  threshold : " << threshold_ << ";\n";
    stream << "  histeresis : " << histeresis_ << ";\n";
    stream << "  state : " << is_enabled_ << ";\n";
    stream << "}\n";
    return stream.readAll();
}
