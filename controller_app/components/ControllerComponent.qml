import QtQuick

Item {
    id: sensors_view

    ListView {
        id: controllers_list

        anchors.top: parent.top
        height: parent.height
        width: 300
        model: gui.getControllers()
        focus: true

        header: Rectangle {
            id: list_header

            width: parent.width
            height: 25
            Text {
                text: "Controllers list"
            }
        }

        delegate: Item {
            id: controllers_list_element

            required property var list_data

            width: parent.width
            height: 80

            Rectangle {
                anchors.fill: parent
                anchors.margins: 2
                color: getStateColor(list_data["state"])
                border.color: "black"

                function getStateColor(state)
                {
                    switch(state)
                    {
                    case "running":
                        return Qt.rgba(0, 1, 0, 1)
                    case "stopped":
                        return Qt.rgba(1, 0, 0, 1)
                    }
                }
            }

            Column {
                anchors.left: parent.left
                anchors.margins: 2
                Text {
                    id: controller_name
                    text: '<b>Id: </b>' + list_data["id"]
                }
                Text {
                    id: controller_type
                    text: '<b>Interval: </b>' + list_data["interval"]
                }
                Text {
                    id: controller_state
                    text: '<b>State: </b>' + list_data["state"]
                }
            }
        }
    }
}
