import QtQuick

Item {
    id: actuators_view

    ListView {
        id: actuators_list

        anchors.top: parent.top
        height: parent.height
        width: 300
        model: gui.getActuators()
        focus: true

        header: Rectangle {
            id: list_header

            width: parent.width
            height: 25
            Text {
                text: "Actuators list"
            }
        }

        delegate: Item {
            id: sensors_list_element

            required property var list_data

            width: parent.width
            height: 80

            Rectangle {
                anchors.fill: parent
                anchors.margins: 2
                color: getStateColor(list_data["state"])
                border.color: "black"

                function getStateColor(state)
                {
                    console.log("state: ", state)
                    switch(state)
                    {
                    case "switch open":
                        return Qt.rgba(1, 0, 0, 1)
                    case "switch close":
                        return Qt.rgba(0, 0, 1, 1)
                    default:
                        return Qt.rgba(1, 1, 1, 1)
                    }
                }
            }

            Column {
                anchors.left: parent.left
                anchors.margins: 2
                Text {
                    id: sensor_name
                    text: '<b>Id: </b>' + list_data["id"]
                }
                Text {
                    id: sensor_type
                    text: '<b>State: </b>' + list_data["state"]
                }
            }
        }
    }
}
