import QtQuick

Item {
    id: sensors_view

    ListView {
        id: sensors_list

        anchors.top: parent.top
        height: parent.height
        width: 300
        model: gui.getSensors()
        focus: true

        header: Rectangle {
            id: list_header

            width: parent.width
            height: 25
            Text {
                text: "Sensors list"
            }
        }

        delegate: Item {
            id: sensors_list_element

            required property var list_data

            width: parent.width
            height: 80

            function getTemperatureColor(temperature)
            {
                var temperature_value = parseFloat(temperature)
                if(temperature_value < 15.0)
                {
                    return Qt.rgba(0, 0, 1, 1);
                }
                if(temperature_value < 16.0)
                {
                    return Qt.rgba(0, 0.33, 1, 1);
                }
                if(temperature_value < 17.0)
                {
                    return Qt.rgba(0, 0.66, 1, 1);
                }
                if(temperature_value < 18.0)
                {
                    return Qt.rgba(0, 1, 0.66, 1);
                }
                if(temperature_value < 19.0)
                {
                    return Qt.rgba(0, 1, 0.33, 1);
                }
                if(temperature_value < 20.0)
                {
                    return Qt.rgba(0, 1, 0, 1);
                }
                if(temperature_value < 21.0)
                {
                    return Qt.rgba(0, 1, 0, 1);
                }
                if(temperature_value < 22.0)
                {
                    return Qt.rgba(0.33, 1, 0, 1);
                }
                if(temperature_value < 23.0)
                {
                    return Qt.rgba(0.66, 1, 0, 1);
                }
                if(temperature_value < 24.0)
                {
                    return Qt.rgba(1, 0.66, 0, 1);
                }
                if(temperature_value < 25.0)
                {
                    return Qt.rgba(1, 0.33, 0, 1);
                }
                if(temperature_value < 26.0)
                {
                    return Qt.rgba(1, 0, 0, 1);
                }
                return Qt.rgba(1, 0, 0, 1);
            }

            // function getTemperatureColor(temperature)
            // {
            //     var temperature_value = parseFloat(temperature)
            //     if(temperature_value < 15.0)
            //     {
            //         return Qt.rgba(0, 0, 250, 1);
            //     }
            //     if(temperature_value < 16.0)
            //     {
            //         return Qt.rgba(0, 100, 250, 1);
            //     }
            //     if(temperature_value < 17.0)
            //     {
            //         return Qt.rgba(0, 200, 250, 1);
            //     }
            //     if(temperature_value < 18.0)
            //     {
            //         return Qt.rgba(0, 250, 200, 1);
            //     }
            //     if(temperature_value < 19.0)
            //     {
            //         return Qt.rgba(0, 250, 100, 255);
            //     }
            //     if(temperature_value < 20.0)
            //     {
            //         return Qt.rgba(0, 250, 0, 255);
            //     }
            //     if(temperature_value < 21.0)
            //     {
            //         return Qt.rgba(0, 250, 0, 255);
            //     }
            //     if(temperature_value < 22.0)
            //     {
            //         return Qt.rgba(100, 250, 0, 255);
            //     }
            //     if(temperature_value < 23.0)
            //     {
            //         return Qt.rgba(200, 250, 0, 255);
            //     }
            //     if(temperature_value < 24.0)
            //     {
            //         return Qt.rgba(250, 200, 0, 255);
            //     }
            //     if(temperature_value < 25.0)
            //     {
            //         return Qt.rgba(250, 100, 0, 255);
            //     }
            //     if(temperature_value < 26.0)
            //     {
            //         return Qt.rgba(250, 0, 0, 255);
            //     }
            //     return Qt.rgba(250, 0, 0, 255);
            // }

            Rectangle {
                anchors.fill: parent
                anchors.margins: 2
                color: getTemperatureColor(list_data["value"])
                border.color: "black"
            }

            Column {
                anchors.left: parent.left
                anchors.margins: 2
                Text {
                    id: sensor_name
                    text: '<b>Id: </b>' + list_data["id"]
                }
                Text {
                    id: sensor_type
                    text: '<b>Type: </b>' + list_data["type"]
                }
                Text {
                    id: sensor_value
                    text: '<b>Type: </b>' + list_data["value"]
                }
                Text {
                    id: sensor_unit
                    text: '<b>Type: </b>' + list_data["unit"]
                }
            }
        }
    }
}
