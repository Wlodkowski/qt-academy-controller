#ifndef IANALOGSENSOR_HPP
#define IANALOGSENSOR_HPP

class IAnalogSensor
{
public:
    virtual float getSensorValue() const = 0;

    virtual ~IAnalogSensor() = default;
};

#endif // IANALOGSENSOR_HPP
