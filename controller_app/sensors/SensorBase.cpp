#include "SensorBase.hpp"

SensorBase::SensorBase(QString id) :
    id_(id)
{
}

QString SensorBase::printId() const
{
    return id_;
}
