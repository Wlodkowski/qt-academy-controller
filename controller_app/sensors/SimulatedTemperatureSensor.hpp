#ifndef SIMULATEDTEMPERATURESENSOR_HPP
#define SIMULATEDTEMPERATURESENSOR_HPP

#include "IAnalogSensor.hpp"
#include "SensorBase.hpp"

#include <QTimer>

class SimulatedTemperatureSensor :  public QObject, public IAnalogSensor, public SensorBase
{
    Q_OBJECT

public:
    SimulatedTemperatureSensor(QString name);

    // implementation of IAnalogSensor
    float getSensorValue() const override;

    // implementation of ISensor
    QString printValueType() const override;
    QString printValue() const override;
    QString printUnit() const override;

//signals:
    void onTimerTimeout();

private:
    QString name_;
    float temperature_value_;
    float upper_bound_;
    float lower_bound_;
    bool is_rising_;

    QTimer timer_;
};

#endif // SIMULATEDTEMPERATURESENSOR_HPP
