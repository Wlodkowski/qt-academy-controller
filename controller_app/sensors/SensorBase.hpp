#ifndef SENSORBASE_HPP
#define SENSORBASE_HPP

#include "ISensor.hpp"

#include <QString>

class SensorBase : public ISensor
{
public:
    SensorBase(QString id);
    virtual ~SensorBase() = default;

    QString printId() const override;

private:
    QString id_;
};

#endif // ISENSORBASE_HPP
