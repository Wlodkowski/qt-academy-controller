#include "SimulatedTemperatureSensor.hpp"

#include <QRandomGenerator>

SimulatedTemperatureSensor::SimulatedTemperatureSensor(QString id) :
    SensorBase(id),
    temperature_value_(20.0f),
    upper_bound_(26.0f),
    lower_bound_(14.0f),
    is_rising_(true),
    timer_()
{
    QRandomGenerator* rng(QRandomGenerator::system());

    temperature_value_ = static_cast<float>(rng->bounded(14, 26)) + 0.1 * static_cast<float>(rng->bounded(0, 9));

    timer_.setInterval(800);   // 800ms
    timer_.callOnTimeout(this, &SimulatedTemperatureSensor::onTimerTimeout);
    timer_.start();
}

float SimulatedTemperatureSensor::getSensorValue() const
{
    return temperature_value_;
}

QString SimulatedTemperatureSensor::printValueType() const
{
    return "temperature";
}

QString SimulatedTemperatureSensor::printValue() const
{
    return QString::number(temperature_value_);
}

QString SimulatedTemperatureSensor::printUnit() const
{
    return "deg C";
}

void SimulatedTemperatureSensor::onTimerTimeout()
{
    if(is_rising_)
    {
        temperature_value_ += 0.3f;
        if(temperature_value_ >= upper_bound_)
        {
            is_rising_ = false;
        }
    }
    else
    {
        temperature_value_ -= 0.3f;
        if(temperature_value_ <= lower_bound_)
        {
            is_rising_ = true;
        }
    }
}
