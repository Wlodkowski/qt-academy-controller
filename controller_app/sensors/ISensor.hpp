#ifndef ISENSOR_HPP
#define ISENSOR_HPP

#include <QString>

class ISensor
{
public:
    virtual QString printId() const = 0;
    virtual QString printValueType() const = 0;
    virtual QString printValue() const = 0;
    virtual QString printUnit() const = 0;

    virtual ~ISensor() = default;
};

#endif // ISENSOR_HPP
